package io.dflabs.dflabscinema.model.management.daos

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import io.dflabs.dflabscinema.library.utils.Preferences
import io.dflabs.dflabscinema.model.objects.Movie

/**
 * Created by danielgarciaalvarado on 5/12/18.
 * DflabsCinema
 */
@Database(entities = arrayOf(
        Movie::class
), version = 1)
abstract class MovieDatabase : RoomDatabase() {

    abstract fun movieDao() : MovieDao

    companion object {
        private var INSTANCE: MovieDatabase? = null

        fun getInstance(context: Context): MovieDatabase? {
            if (INSTANCE == null) {
                synchronized(MovieDatabase::class.java) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                            MovieDatabase::class.java, "movies.db")
                            .allowMainThreadQueries()
                            .build()
                }
            }
            return INSTANCE
        }
    }
}