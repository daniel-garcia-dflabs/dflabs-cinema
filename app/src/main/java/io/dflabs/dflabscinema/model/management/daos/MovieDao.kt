package io.dflabs.dflabscinema.model.management.daos

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import android.arch.persistence.room.Transaction
import io.dflabs.dflabscinema.model.objects.Movie

/**
 * Created by danielgarciaalvarado on 5/12/18.
 * DflabsCinema
 */
@Dao
interface MovieDao {

    @Query("SELECT * FROM movie")
    fun movies() : List<Movie>

    @Query("SELECT * FROM movie WHERE id = :id")
    fun movie(id : Long) : Movie

    @Insert(onConflict = REPLACE)
    fun insertOrUpdate(movie: Movie)

    @Query("DELETE FROM movie")
    fun deleteAll()

    @Query("DELETE FROM movie WHERE id = :id")
    fun delete(id: Long)

    /*
    @Transaction
    fun longMethodWithTransaction() {

    } */
}