package io.dflabs.dflabscinema.library.utils

import android.content.Context
import android.content.SharedPreferences
import io.dflabs.dflabscinema.BuildConfig

/**
 * Created by danielgarciaalvarado on 5/5/18.
 * DflabsCinema
 */
class Preferences(context: Context) {


    private var sharedPreferences: SharedPreferences? = null

    init {
        sharedPreferences = context.applicationContext.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE)
    }

    companion object {
        private var INSTANCE: Preferences? = null

        fun getInstance(context: Context): Preferences? {
            if (INSTANCE == null) {
                synchronized(Preferences::class.java) {
                    INSTANCE = Preferences(context)
                }
            }
            return INSTANCE
        }
    }


    fun putString(key: String, value : String?) {
        sharedPreferences?.edit()?.putString(key, value)?.apply()
    }

    fun getString(key: String): String? {
        return sharedPreferences?.getString(key, null)
    }
}