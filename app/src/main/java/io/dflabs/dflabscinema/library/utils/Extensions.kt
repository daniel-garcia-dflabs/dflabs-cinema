package io.dflabs.dflabscinema.library.utils

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions

/**
 * Created by danielgarciaalvarado on 5/4/18.
 * DflabsCinema
 */
fun ImageView.download(url: String?) {
    Glide.with(context)
            .load(url)
            .apply(RequestOptions()
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL))
            .into(this)
}