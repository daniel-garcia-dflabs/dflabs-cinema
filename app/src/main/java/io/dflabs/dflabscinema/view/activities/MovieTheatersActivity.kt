package io.dflabs.dflabscinema.view.activities

import android.app.ProgressDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import io.dflabs.dflabscinema.R
import io.dflabs.dflabscinema.model.objects.MovieTheater
import io.dflabs.dflabscinema.presenter.callbacks.MoviesTheaterCallback
import io.dflabs.dflabscinema.presenter.implementations.MoviesTheaterPresenter

class MovieTheatersActivity : AppCompatActivity(), OnMapReadyCallback, MoviesTheaterCallback {

    private lateinit var mMap: GoogleMap
    var moviesTheaterPresenter : MoviesTheaterPresenter? = null
    var progressDialog : ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_theaters)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        moviesTheaterPresenter = MoviesTheaterPresenter(this)
        this.progressDialog = ProgressDialog.show(this, null, getString(R.string.dialog_loading), true)
        progressDialog?.hide()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(19.0, -99.0), 17.0F))
        moviesTheaterPresenter?.fetchMovieTheaters(this)

        initializeEvents()
    }

    private fun initializeEvents() {
        mMap.setOnMarkerClickListener {
            print(it.title)
            return@setOnMarkerClickListener true
        }

        mMap.setOnMarkerDragListener(object : GoogleMap.OnMarkerDragListener{
            override fun onMarkerDragEnd(marker: Marker?) {
                print(marker?.position)
            }

            override fun onMarkerDragStart(p0: Marker?) {

            }

            override fun onMarkerDrag(p0: Marker?) {

            }
        })
    }

    override fun onLoadingMoviesTheaters() {
        progressDialog?.show()
    }

    override fun onSuccessMoviesTheaters(movieTheaters: ArrayList<MovieTheater>) {
        progressDialog?.hide()
        movieTheaters.forEach({
            mMap.addMarker(
                    MarkerOptions()
                            .position(LatLng(it.latitude, it.longitude))
                            .title(it.name)
                            .draggable(true))
        })
    }

    override fun onErrorFetchingMoviesTheaters() {
        progressDialog?.hide()
        Toast.makeText(this, R.string.dialog_error_loading_movie_theaters, Toast.LENGTH_LONG).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        moviesTheaterPresenter?.onDestroy()
    }
}
