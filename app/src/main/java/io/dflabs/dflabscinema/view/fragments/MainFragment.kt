package io.dflabs.dflabscinema.view.fragments

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import io.dflabs.dflabscinema.R
import io.dflabs.dflabscinema.background.bus.events.UpdateMoviesEvent
import io.dflabs.dflabscinema.library.utils.Keys
import io.dflabs.dflabscinema.library.utils.Preferences
import io.dflabs.dflabscinema.model.objects.Movie
import io.dflabs.dflabscinema.presenter.callbacks.MoviesCallback
import io.dflabs.dflabscinema.presenter.implementations.MoviesPresenter
import io.dflabs.dflabscinema.view.activities.MovieTheatersActivity
import io.dflabs.dflabscinema.view.adapters.MovieAdapter
import kotlinx.android.synthetic.main.fragment_main.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

/**
 * Created by danielgarciaalvarado on 4/28/18.
 * DflabsCinema
 */
class MainFragment : Fragment(), MoviesCallback {


    var moviesPresenter : MoviesPresenter? = null
    var adapter : MovieAdapter? = null
    var emptyView : View? = null
    var swipeRefreshLayout : SwipeRefreshLayout? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val recyclerView = view.fr_main_trending
        val recyclerViewNew = view.fr_main_new
        emptyView = view.fr_main_empty
        swipeRefreshLayout = view.fr_main_swipe_refresh_layout

        emptyView?.setOnClickListener {
            moviesPresenter?.fetchMovies(context!!)
        }

        swipeRefreshLayout?.setOnRefreshListener {
            moviesPresenter?.fetchMoviesRx(context!!)
        }

        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerViewNew.itemAnimator = DefaultItemAnimator()



        adapter = MovieAdapter(ArrayList())

        recyclerView.adapter = adapter
        recyclerViewNew.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        recyclerViewNew.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)

        val logoImageView = view.fr_main_image_logo as ImageView
        logoImageView.setOnClickListener {
            /*
            Preferences.getInstance(context!!)?.putString(Keys.PREF_TOKEN, null)
            activity?.finish()
            */
            startActivity(Intent(context, MovieTheatersActivity::class.java))
        }

        moviesPresenter = MoviesPresenter(this)
        moviesPresenter?.fetchMovies(context!!)
    }

    override fun onResume() {
        super.onResume()
        EventBus.getDefault().register(this)
    }

    override fun onPause() {
        super.onPause()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onUpdateMoviesEvent(event: UpdateMoviesEvent){
        moviesPresenter?.fetchMovies(context!!)
    }

    override fun onDestroy() {
        super.onDestroy()
        moviesPresenter?.onDestroy()
    }

    override fun onLoadingMovies(localMovies: ArrayList<Movie>) {
        adapter?.update(localMovies)
    }

    override fun onSuccessMovies(movies: ArrayList<Movie>) {
        updateView(movies)
    }

    override fun onErrorFetchingMovies(localMovies: ArrayList<Movie>) {
        updateView(localMovies)
        val snackBar = Snackbar.make(view!!, R.string.dialog_error_loading_movies, Snackbar.LENGTH_INDEFINITE)
        snackBar.setAction(R.string.action_retry) {
            snackBar.dismiss()
            moviesPresenter?.fetchMovies(context!!)
        }

        snackBar.view.setBackgroundColor(ContextCompat.getColor(context!!, android.R.color.white))
        snackBar.show()
    }

    fun updateView(movies: ArrayList<Movie>) {
        swipeRefreshLayout?.isRefreshing = false
        if (movies.size > 0) {
            swipeRefreshLayout?.visibility = View.VISIBLE
            adapter?.update(movies)
        } else {
            swipeRefreshLayout?.visibility = View.GONE
            emptyView?.visibility = View.VISIBLE
        }
    }
}