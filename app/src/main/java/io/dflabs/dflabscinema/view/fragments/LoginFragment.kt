package io.dflabs.dflabscinema.view.fragments

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.TextInputLayout
import android.support.v4.app.Fragment
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import io.dflabs.dflabscinema.R
import io.dflabs.dflabscinema.presenter.callbacks.LoginCallback
import io.dflabs.dflabscinema.presenter.implementations.LoginPresenter
import io.dflabs.dflabscinema.view.activities.MainActivity
import kotlinx.android.synthetic.main.fragment_login.view.*


/**
 * Created by danielgarciaalvarado on 4/28/18.
 * DflabsCinema
 */
class LoginFragment : Fragment(), LoginCallback {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    val loginPresenter = LoginPresenter(this)
    var progressDialog : ProgressDialog? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.progressDialog = ProgressDialog.show(context, null, getString(R.string.dialog_loading), true)
        progressDialog?.hide()

        val usernameEditText = view.fr_login_username as EditText
        val passwordEditText = view.fr_login_password as EditText

        val usernameTextInputLayout = view.fr_login_username_til as TextInputLayout
        val passwordTextInputLayout = view.fr_login_password_til as TextInputLayout

        val continueButton = view.fr_login_continue as FloatingActionButton

        continueButton.setOnClickListener({
            if (TextUtils.isEmpty(usernameEditText.text.trim())){
                usernameTextInputLayout.error = getString(R.string.dialog_error_empty)
                return@setOnClickListener
            }
            usernameTextInputLayout.error = null

            if (TextUtils.isEmpty(passwordEditText.text.trim())){
                passwordTextInputLayout.error = getString(R.string.dialog_error_empty)
                return@setOnClickListener
            }
            passwordTextInputLayout.error = null

            loginPresenter.login(
                    usernameEditText.text.trim().toString(),
                    passwordEditText.text.trim().toString(),
                    context!!
            )
        })
    }

    fun openMainActiviy() {
        val intent = Intent(context, MainActivity::class.java)
        startActivity(intent)
        activity?.finish()
    }

    override fun onLoadingLogin() {
        progressDialog?.show()
    }

    override fun onSuccessLogin() {
        progressDialog?.hide()
        openMainActiviy()
    }

    override fun onErrorLogin(error: Int) {
        progressDialog?.hide()
        when (error) {
            400, 500 -> Toast.makeText(context, R.string.dialog_server_error, Toast.LENGTH_LONG).show()
            401 -> Toast.makeText(context, R.string.dialog_incorrect_credentials, Toast.LENGTH_LONG).show()
            404 -> Toast.makeText(context, R.string.dialog_user_not_found, Toast.LENGTH_LONG).show()
            else -> Toast.makeText(context, R.string.dialog_unknown_error, Toast.LENGTH_LONG).show()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        loginPresenter.onDestroy()
    }
}