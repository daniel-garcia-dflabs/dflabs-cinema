package io.dflabs.dflabscinema.view.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import io.dflabs.dflabscinema.R
import io.dflabs.dflabscinema.library.utils.download
import io.dflabs.dflabscinema.model.objects.Movie

/**
 * Created by danielgarciaalvarado on 4/28/18.
 * DflabsCinema
 */
class MovieAdapter(var movies: ArrayList<Movie>) : RecyclerView.Adapter<MovieAdapter.MovieViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_movie, parent, false)
        return MovieViewHolder(view)
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val movie = movies[position]
        holder.imageView?.download(movie.image)
        holder.titleTextView?.text = movie.title
        holder.classificationTextView?.text = movie.classification ?: ""
    }

    fun update(movies: ArrayList<Movie>) {
        this.movies = movies
        notifyDataSetChanged()
    }

    class MovieViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val imageView = itemView?.findViewById<ImageView>(R.id.item_movie_image)
        val titleTextView = itemView?.findViewById<TextView>(R.id.item_movie_title)
        val classificationTextView = itemView?.findViewById<TextView>(R.id.item_movie_classification)
    }
}