package io.dflabs.dflabscinema.view.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import io.dflabs.dflabscinema.R
import io.dflabs.dflabscinema.library.utils.Keys
import io.dflabs.dflabscinema.library.utils.Preferences

/**
 * Created by danielgarciaalvarado on 4/28/18.
 * DflabsCinema
 */
class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val token = Preferences.getInstance(this)?.getString(Keys.PREF_TOKEN)
        token?.let {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }

        setContentView(R.layout.activity_login)
    }
}