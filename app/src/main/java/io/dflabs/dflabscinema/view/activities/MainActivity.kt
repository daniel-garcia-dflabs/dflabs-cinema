package io.dflabs.dflabscinema.view.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import io.dflabs.dflabscinema.library.Kotlin
import io.dflabs.dflabscinema.library.Kotlin2
import io.dflabs.dflabscinema.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
