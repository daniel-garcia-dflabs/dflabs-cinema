package io.dflabs.dflabscinema.presenter.implementations

import android.content.Context
import io.dflabs.dflabscinema.background.ws.WebServices
import io.dflabs.dflabscinema.library.utils.Keys
import io.dflabs.dflabscinema.library.utils.Preferences
import io.dflabs.dflabscinema.model.management.daos.MovieDatabase
import io.dflabs.dflabscinema.model.objects.Movie
import io.dflabs.dflabscinema.presenter.callbacks.MoviesCallback
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by danielgarciaalvarado on 5/5/18.
 * DflabsCinema
 */
class MoviesPresenter(val callback: MoviesCallback) {

    private var call: Call<ArrayList<Movie>>? = null
    private var disposable: Disposable? = null
    private var db : MovieDatabase? = null

    fun fetchMovies(context: Context) {
        if (db == null){
            db = MovieDatabase.getInstance(context)
        }
        val localMovies = db?.movieDao()?.movies() as ArrayList
        callback.onLoadingMovies(localMovies)
        val token = Preferences.getInstance(context)?.getString(Keys.PREF_TOKEN)
        if (token == null) {
            callback.onErrorFetchingMovies(localMovies)
            return
        }
        call = WebServices.api.movies("Token ${token}")
        call?.enqueue(object : Callback<ArrayList<Movie>> {
            override fun onFailure(call: Call<ArrayList<Movie>>?, t: Throwable?) {
                callback.onErrorFetchingMovies(localMovies)
            }

            override fun onResponse(call: Call<ArrayList<Movie>>?, response: Response<ArrayList<Movie>>?) {
                if (response?.isSuccessful != null && response.isSuccessful) {
                    val movies = response.body()
                    movies?.size!! > 0.apply {
                        db?.movieDao()?.deleteAll()
                        movies.forEach {
                            db?.movieDao()?.insertOrUpdate(it)
                        }
                    }
                    callback.onSuccessMovies(movies ?: ArrayList())
                } else {
                    callback.onErrorFetchingMovies(localMovies)
                }
            }
        })
    }

    fun fetchMoviesRx(context: Context) {
        if (db == null){
            db = MovieDatabase.getInstance(context)
        }
        val localMovies = db?.movieDao()?.movies() as ArrayList
        val token = Preferences.getInstance(context)?.getString(Keys.PREF_TOKEN)
        if (token == null) {
            callback.onErrorFetchingMovies(localMovies)
            return
        }
        disposable = WebServices.apiRx.movies("Token ${token}")
                .doOnSubscribe({callback.onLoadingMovies(localMovies)})
                .observeOn(AndroidSchedulers.mainThread())
                .map {
                    it.size > 0.apply {
                        db?.movieDao()?.deleteAll()
                        it.forEach {
                            db?.movieDao()?.insertOrUpdate(it)
                        }
                    }
                    return@map it
                }
                .subscribe({
                    callback.onSuccessMovies(it)
                }, {
                    callback.onErrorFetchingMovies(localMovies)
                })
    }

    fun onDestroy() {
        call?.let {
            /*it.isExecuted.apply {
                it.cancel()
            }*/
            if (it.isExecuted) {
                it.cancel()
            }
        }

        disposable?.dispose()
    }
}