package io.dflabs.dflabscinema.presenter.callbacks

import io.dflabs.dflabscinema.model.objects.MovieTheater

interface MoviesTheaterCallback {
    fun onLoadingMoviesTheaters()

    fun onSuccessMoviesTheaters(movieTheaters: ArrayList<MovieTheater>)

    fun onErrorFetchingMoviesTheaters()
}
