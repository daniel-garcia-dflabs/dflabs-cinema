package io.dflabs.dflabscinema.background.ws.definitions

import io.dflabs.dflabscinema.model.objects.Movie
import io.dflabs.dflabscinema.model.objects.MovieTheater
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header

/**
 * Created by danielgarciaalvarado on 5/5/18.
 * DflabsCinema
 */
interface ApiDefinition {

    @GET("/api/v1/movies/")
    fun movies(@Header("Authorization") authorization : String) : Call<ArrayList<Movie>>

    @GET("/api/v1/movie-theaters/")
    fun movieTheaters(@Header("Authorization") authorization : String) : Call<ArrayList<MovieTheater>>

}