package io.dflabs.dflabscinema.background.ws

import io.dflabs.dflabscinema.BuildConfig
import io.dflabs.dflabscinema.background.ws.definitions.AccountsDefinition
import io.dflabs.dflabscinema.background.ws.definitions.ApiDefinition
import io.dflabs.dflabscinema.background.ws.definitions.ApiDefinitionRx
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by danielgarciaalvarado on 5/5/18.
 * DflabsCinema
 */
object WebServices {
    val accounts = Retrofit.Builder()
            .baseUrl(BuildConfig.SERVER_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(AccountsDefinition::class.java)

    val api = Retrofit.Builder()
            .baseUrl(BuildConfig.SERVER_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiDefinition::class.java)

    val apiRx = Retrofit.Builder()
            .baseUrl(BuildConfig.SERVER_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
            .create(ApiDefinitionRx::class.java)
}
