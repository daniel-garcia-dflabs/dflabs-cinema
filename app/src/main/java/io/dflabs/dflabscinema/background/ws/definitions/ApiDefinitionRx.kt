package io.dflabs.dflabscinema.background.ws.definitions

import io.dflabs.dflabscinema.background.ws.requests.TokenRequest
import io.dflabs.dflabscinema.background.ws.responses.TokenResponse
import io.dflabs.dflabscinema.model.objects.Movie
import io.reactivex.Flowable
import org.intellij.lang.annotations.Flow
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

/**
 * Created by danielgarciaalvarado on 5/12/18.
 * DflabsCinema
 */
interface ApiDefinitionRx {

    @GET("/api/v1/movies/")
    fun movies(@Header("Authorization") authorization : String) : Flowable<ArrayList<Movie>>

    @POST("/api/v1/token/register/")
    fun sendToken(@Body tokenRequest: TokenRequest,
                  @Header("Authorization") authorization : String): Flowable<TokenResponse>

}