package io.dflabs.dflabscinema.background.ws.definitions

import io.dflabs.dflabscinema.background.ws.requests.LoginRequest
import io.dflabs.dflabscinema.background.ws.responses.LoginResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query

/**
 * Created by danielgarciaalvarado on 5/5/18.
 * DflabsCinema
 */
interface AccountsDefinition {

    @POST("/api/v1/login/")
    fun login(@Body loginRequest: LoginRequest) : Call<LoginResponse>
}