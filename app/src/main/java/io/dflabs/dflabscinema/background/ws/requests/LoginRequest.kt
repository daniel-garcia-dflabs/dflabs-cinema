package io.dflabs.dflabscinema.background.ws.requests

data class LoginRequest(val username: String, val password: String)
